require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do

  describe 'instructions' do
    let!(:user) { create(:user) }
    let!(:second_user) { create(:second_user) }
    let(:email) { build :email, sender: user.email, users: [second_user], subject: "subject", body: "body" }
    let(:mail) { UserMailer.send_mail(email).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('subject')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([second_user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq([user.email])
    end

    it 'assigns @name' do
      expect(mail.body.encoded).to match("body")
    end
  end
end
