require 'rails_helper'

RSpec.describe Email, type: :model do

  let!(:user) { create(:user) }
  let!(:email) { build(:email, users: [user.id]) }

  it "has a valid email" do
    expect(email).to be_valid
  end

  it "is invalid without sender" do
    expect(build(:email, users:[user.id], sender: nil, subject: "test")).not_to be_valid
  end

  it "is invalid without users" do
    expect(build(:email, users: nil, sender: user.id, subject: "test")).not_to be_valid
  end

  it "is valid without subject" do
    expect(build(:email, users: nil, sender: user.id)).not_to be_valid
  end

  it "is valid without body" do
    expect(build(:email, users: nil, sender: user.id)).not_to be_valid
  end

end
