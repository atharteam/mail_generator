require 'rails_helper'

RSpec.describe User, type: :model do

  let!(:user) { create(:user) }

  it "has a valid user" do
    expect(user).to be_valid
  end

  it "is invalid without name" do
    expect(build(:user, email: "new_user@mail_generator.com", name: nil)).not_to be_valid
  end

  it "returns email as a string" do
    expect(user.email).to match 'test@mail_generator.com'
  end

  it "returns name as a string" do
    expect(user.name).to match 'Test'
  end

end
