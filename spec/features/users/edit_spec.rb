feature 'Edit Profile' do

  scenario 'user changes email address' do
    user = FactoryGirl.create(:user)
    login_as(user)
    visit edit_user_registration_path
    fill_in 'user_email', with: 'newemail@example.com'
    fill_in 'user_current_password', with: user.password
    click_button 'Update Account'
    expect(page).to have_content('Your account has been updated successfully.')
  end


  scenario "user cannot cannot update profile with password not matching confirming password", :me do
    user = FactoryGirl.create(:user)
    login_as(user)
    visit edit_user_registration_path
    fill_in 'user_current_password', with: user.password
    fill_in 'user_password', with: "test_123"
    fill_in 'user_password_confirmation', with: "test_12345"
    click_button 'Update Account'
    expect(page).to have_content("Password confirmation doesn't match Password")
  end

end
