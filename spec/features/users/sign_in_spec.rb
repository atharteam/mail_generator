require 'rails_helper'

feature 'Sign in' do

  scenario 'user cannot sign in if unregistered' do
    sign_in_user('invalid_user@mail_generator.com', 'test123')
    expect(page).to have_content 'Invalid Email or password.'
  end

  feature 'invalid credentials' do
    let!(:user) { create(:user) }

    scenario 'user sign in if registered' do
      sign_in_user(user.email, user.password)
      expect(page).to have_content 'Signed in successfully.'
    end

    scenario 'user not sign in if invalid email' do
      sign_in_user("invalid_user@mail_generator", user.password)
      expect(page).to have_content 'Invalid Email or password.'
    end

    scenario 'user not sign in if invalid password' do
      sign_in_user(user.email, "wrong_password")
      expect(page).to have_content 'Invalid Email or password.'
    end
  end

end
