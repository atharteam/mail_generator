require 'rails_helper'

feature 'User index page' do

  scenario 'user can not sees index page without login' do
    visit users_path
    expect(page).to have_content 'Log In'
  end


  feature 'logged in user' do

    let!(:user) { create(:user) }
    let!(:users) { 50.times.map { |index| User.create!(name: "dummy_user_#{index}", email: "dummy_user_#{index}@mail_generator.com", password: '123456' ) } }

    before(:each) do
      login_as(user)
      visit users_path
    end

    scenario 'user sees log out button' do
      expect(page).to have_content 'Users'
    end

    scenario 'users sees the list of ten users on first page in the system' do
      expect(page).to have_xpath(".//tr", count: 10)
    end

    scenario 'users does not see any user on invalid page in the system' do
      visit users_path(page: 10000)
      expect(page).to have_xpath(".//tr", count: 0)
    end


  end

end
