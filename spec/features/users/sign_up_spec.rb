require 'rails_helper'

feature 'Sign up' do
  let!(:user) { create(:user, email: 'new_user@mail_generator') }

  scenario 'user cannot sign up if name is blank' do
    sign_up_with('', 'invalid_user@mail_generator', 'test12345', 'test12345')
    expect(page).to have_content "Name can't be blank"
  end

  scenario 'user cannot sign up if email is blank' do
    sign_up_with('Test User', '', 'test12345', 'test12345')
    expect(page).to have_content "Email can't be blank"
  end

  scenario 'user cannot sign up if email is not a valid format' do
    sign_up_with('Test User', 'test', 'test12345', 'test12345')
    expect(page).to have_content "Email is invalid"
  end

  scenario 'user cannot sign up if email is already taken' do
    sign_up_with('Test User', 'new_user@mail_generator', 'test12345', 'test12345')
    expect(page).to have_content "Email has already been taken"
  end

  scenario 'user cannot sign up if password is short' do
    sign_up_with('Test User', 'invalid_user@mail_generator', '123', '123')
    expect(page).to have_content "Password is too short"
  end

  scenario 'user cannot sign up if passwords does not match' do
    sign_up_with('Test User', 'invalid_user@mail_generator', 'test12345', 'test')
    expect(page).to have_content "Password confirmation doesn't match Password"
  end

  scenario 'user can sign up if credentials are valid' do
    sign_up_with('Test User', 'valid_user@mail_generator.com', 'test123', 'test123')
    expect(page).to have_content "Welcome! You have signed up successfully."
  end

end
