require 'rails_helper'

feature 'Sign out' do

  scenario 'user signs out successfully' do
    user = FactoryGirl.create(:user)
    sign_in_user(user.email, user.password)
    click_link 'Sign Out'
    expect(page).to have_content 'Signed out successfully.'
  end

end


