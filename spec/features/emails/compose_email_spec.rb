require 'rails_helper'

feature 'Compose Email' do

  feature 'for valid recipients' do
    let!(:user) { create(:user) }
    let!(:second_user) { create(:second_user) }

    before(:each) do
      sign_in_user(user.email, user.password)
    end

    scenario 'user add email subject and body successfully and trigger emails' do
      visit "/emails/compose/#{second_user.id}"
      fill_in 'Subject', with: "subject"
      fill_in 'Body', with: "body"
      click_button 'Send'
      expect(page).to have_content 'Email has been sent'
    end
  end

  feature 'for invalid recipients' do
    let!(:user) { create(:user) }
    let!(:second_user) { create(:second_user) }

    before(:each) do
      sign_in_user(user.email, user.password)
    end

    scenario 'user add email subject and body successfully and trigger emails' do
      visit "/emails/compose/#{Random.rand(11000)}"
      expect(page).to have_content 'Please select some valid Recipient'
    end
  end

end
