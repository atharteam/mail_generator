RSpec.configure do |config|
  config.include Devise::TestHelpers, type: :controller
  config.include Features::SessionHelper
end
