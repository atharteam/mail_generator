FactoryGirl.define do
  factory :email, class: Email do
    sender "sender@mail_generator.com"
    recipients 'receiver@mail_generator.com'
    subject 'subject'
    body 'body'
  end

end
