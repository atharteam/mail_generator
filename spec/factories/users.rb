FactoryGirl.define do
  factory :user, class: User do
    name "Test"
    email 'test@mail_generator.com'
    password '12345678'
    password_confirmation '12345678'

    factory :second_user, parent: :user do
      name "Test_2"
      email 'test_2@mail_generator.com'
    end

    factory :third_user, parent: :user do
      name "Test_3"
      email 'test_3@mail_generator.com'
    end

  end

end
