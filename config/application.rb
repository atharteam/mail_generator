require_relative 'boot'

require 'rails/all'


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MailGenerator
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end

raw_config = File.read("#{Rails.root}/config/app_config.yml")
APP_CONFIG = ActiveSupport::HashWithIndifferentAccess.new YAML.load(raw_config)[Rails.env]
