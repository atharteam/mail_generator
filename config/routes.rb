Rails.application.routes.draw do
  devise_for :users

  resources :users, only: [:index] do
    get 'page(/:page)', action: :index, on: :collection
  end

  resources :emails, only: [:create] do
    collection do
      get 'compose/:users', action: :compose, as: :compose
      post 'bulk_compose'
    end
  end

  devise_scope :user do
    authenticated :user do
      root 'users#index', as: :root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  get '*path', to: 'application#raise_not_found'

end
