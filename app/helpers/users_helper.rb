module UsersHelper

  def pagination_list
    [10, 15, 20, 25, 50, 100]
  end

  def selected_limit(params)
    params[:limit] || Kaminari.config.default_per_page
  end

end
