class EmailsController < ApplicationController

  before_action :authenticate_user!
  before_action :validate_recipients, only: [:compose, :bulk_compose]
  before_action :initialise_email, only: [:compose, :bulk_compose]

  def compose
  end

  def bulk_compose
    render 'compose'
  end

  def create
    @email = Email.new(email_params)
    @email.sender = current_user.email

    if @email.valid?
      UserMailer.send_mail(@email).deliver
       flash[:notice] = "Email has been sent"
    else
      flash[:error] = "Email can not be sent"
    end
    redirect_to :root
  end

  private

  def validate_recipients
    @recipients = User.where(id: params[:users])
    return redirect_to :root, alert: 'Please select some valid Recipient' if @recipients.blank?
  end

  def initialise_email
    @email = Email.new
  end

  def email_params
    params.require(:email).permit(:subject, :body, users: [])
  end

end
