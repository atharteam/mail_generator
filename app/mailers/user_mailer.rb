class UserMailer < ApplicationMailer

  def send_mail(email)
    @email = email
    to = email.recipients
    from = email.sender
    subject = email.subject
    mail to: to, from: from, subject: subject
  end

end
