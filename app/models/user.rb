class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true, length: { maximum: 80 }

  scope :all_except, ->(user) { where.not(id: user.id) }
  scope :email_for_ids, ->(ids) { where(id: ids).compact.pluck(:email) }

end
