class Email

  include ActiveModel::Model

  attr_accessor :recipients, :subject, :body, :users, :sender

  validates :sender, presence: true
  validates :users, presence: true

  def recipients
    User.email_for_ids(self.users)
  end

  def users=(values)
    @users = User.where(id: values).pluck(:id).compact
  end

end
