select_all_checkboxes = ->
  $('.users-checkbox').prop 'checked', true
  $('.users-checkbox').each ->
    $(this).val $(this).parents('tr').find('.user-id').val()

unselect_all_checkboxes = ->
  $('.users-checkbox').prop 'checked', false
  $('.users-checkbox').each ->
    $(this).val 0

reset_all_checkboxes = ->
  $('input:checkbox').removeAttr('checked');

hide_bulk_send = ->
  $('.bulk-send').hide()

show_bulk_send = ->
  $('.bulk-send').show()

enable_compose_button = ->
  $('.bulk-compose').prop('disabled', false) if $('.users-checkbox:checked').length > 0

disable_compose_button = ->
  $('.bulk-compose').prop('disabled', true) if $('.users-checkbox:checked').length <= 0

select_all_recipients = ->
  $('.toggle-in-bulk').click ->
    if $(this).is(':checked')
      show_bulk_send()
      select_all_checkboxes()
      enable_compose_button()
    else
      hide_bulk_send()
      unselect_all_checkboxes()
      disable_compose_button()

select_single_checkbox = ->
  $('.users-checkbox').click ->
    if $(this).is(':checked')
      show_bulk_send()
      enable_compose_button()
      $(this).val $(this).parents('tr').find('.user-id').val()
    else
      hide_bulk_send()
      disable_compose_button()
      $(this).val 0

bind_per_page_change = ->
  $('#limit').change ->
    $('#users-limit-form').submit();

$(document).on 'turbolinks:load', ->
  reset_all_checkboxes()
  hide_bulk_send()
  select_all_recipients()
  select_single_checkbox()
  bind_per_page_change()
